package com.tesis.uagrmar.model;

/**
 * Created by ticketeg on 02-03-17.
 */

public class Category {
    String nombre = "";

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
