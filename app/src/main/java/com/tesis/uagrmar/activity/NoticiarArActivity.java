package com.tesis.uagrmar.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tesis.uagrmar.R;
import com.wikitude.architect.ArchitectStartupConfiguration;
import com.wikitude.architect.ArchitectView;

import java.io.IOException;

public class NoticiarArActivity extends AppCompatActivity {

    ArchitectView architectView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticiar_ar);

        this.architectView = (ArchitectView) this.findViewById(R.id.architectView);
        final ArchitectStartupConfiguration config = new ArchitectStartupConfiguration();
        config.setLicenseKey("2sc7UXzHOayYFKRy08V7yNo4plZUBUxNSiSGTFs6EzOzEMmnibWKfKGYP52tsLTzzfJv6YAchZZFW9MT0I+paO2Jk4S7rTapwYDU8FhX9FJgXX39+iyZ2mt2EAMiucnqa0hce9xW6zvqqFMaxkIx/SnI5xRcg9mo5OtJwyDCl61TYWx0ZWRfX9X/fPMIhJqSHuxhDanYUs0lPiDDHgZf1KVUD4GVh+PjKUj8N7TWp2VEkNROJ3PUg9BEMHrxjCRvrJs7wsIiXxinm8LVVy7cNYrFJmMsNHxRRU/SLoD0YgunM0HOMfdDR+JEG2p1PeFtor3RqECbhjtjaXbeMlOSiaJTK0oalfG4DtS4X+O+nA62zpXfdmaabOj4TdybP1tGwsXM16g1CekiOXUQcVZ5pMcHkbRDQPNUmwzFS7eT46DPQ6DjyXykMCzP0d3FnpciMDc/uwn+7v9URBUc9SWj+bdtyCTqtBEPE4Lpa+YxdbT3ZPhjQROMRnpBpWHUBZvLFmHcRi5Qik2dxXuQuvCikxBL7Rba4zsLYH7yaoqWiCHAfcHtcOwkublQ4bddC7p9SMR0XlfYcX27avgfpc1snG/WbtB1ZoWBLv2uSKLrPAxoomZ82z2fh3W4pq3Olo1Kq8qZ7wAgLeqttx4biYc7VZYZDUMr5466/ZXYKJ5Mvis=");
        this.architectView.onCreate(config);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.architectView.onPostCreate();
        try {
            this.architectView.load("09_Video_4_Bonus-Transparent$Video/index.html");
            //this.architectView.load("08_Browsing$Pois_5_Native$Detail$Screen/index.html");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.architectView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.architectView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.architectView.onDestroy();
    }

}
