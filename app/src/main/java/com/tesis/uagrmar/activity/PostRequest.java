package com.tesis.uagrmar.activity;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by danae on 23-02-17.
 */

public class PostRequest {
    private Activity activity;
    private JSONArray params;
    private String url;

    public PostRequest(Activity activity, JSONArray params, String url){
        this.activity = activity;
        this.params = params;
        this.url = url;
        //sendPost();
    }

    public void sendPost(final MapsModuleActivity mapactivity) {
        //send by volley library with post method
        final List<Establecimiento> contactList = new ArrayList<Establecimiento>();
        final JSONArray[] result = new JSONArray[1];
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, url, params,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Gson gson = new Gson();
                            Type type = new TypeToken<List<Establecimiento>>(){}.getType();
                            List<Establecimiento> contactList2=gson.fromJson(response.toString(), type);
                            //contactList.addAll(contactList2);
                            mapactivity.obtenerModulos(contactList2);
                            Log.i("info",response.toString());
                            //progress.dismiss();
                            Toast.makeText(activity, "Datos sincronizados exitosamente", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.i("info", "error catch");
                            //progress.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(activity, "No se pudo conectar al servidor", Toast.LENGTH_LONG).show();
                        Log.i("info", error.toString());
                        //progress.dismiss();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        stringRequest.setRetryPolicy(
                new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}
