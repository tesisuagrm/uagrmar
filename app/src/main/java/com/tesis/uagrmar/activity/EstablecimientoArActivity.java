package com.tesis.uagrmar.activity;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.tesis.uagrmar.R;
import com.wikitude.architect.ArchitectStartupConfiguration;
import com.wikitude.architect.ArchitectView;


import java.io.IOException;
import java.util.Random;

public class EstablecimientoArActivity extends AppCompatActivity {

    ArchitectView architectView;

    LocationManager locationManager;
    double latitude = 0;
    double longitude = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_establecimiento_ar);

        this.architectView = (ArchitectView) this.findViewById(R.id.architectView);
        final ArchitectStartupConfiguration config = new ArchitectStartupConfiguration();
        config.setLicenseKey("2sc7UXzHOayYFKRy08V7yNo4plZUBUxNSiSGTFs6EzOzEMmnibWKfKGYP52tsLTzzfJv6YAchZZFW9MT0I+paO2Jk4S7rTapwYDU8FhX9FJgXX39+iyZ2mt2EAMiucnqa0hce9xW6zvqqFMaxkIx/SnI5xRcg9mo5OtJwyDCl61TYWx0ZWRfX9X/fPMIhJqSHuxhDanYUs0lPiDDHgZf1KVUD4GVh+PjKUj8N7TWp2VEkNROJ3PUg9BEMHrxjCRvrJs7wsIiXxinm8LVVy7cNYrFJmMsNHxRRU/SLoD0YgunM0HOMfdDR+JEG2p1PeFtor3RqECbhjtjaXbeMlOSiaJTK0oalfG4DtS4X+O+nA62zpXfdmaabOj4TdybP1tGwsXM16g1CekiOXUQcVZ5pMcHkbRDQPNUmwzFS7eT46DPQ6DjyXykMCzP0d3FnpciMDc/uwn+7v9URBUc9SWj+bdtyCTqtBEPE4Lpa+YxdbT3ZPhjQROMRnpBpWHUBZvLFmHcRi5Qik2dxXuQuvCikxBL7Rba4zsLYH7yaoqWiCHAfcHtcOwkublQ4bddC7p9SMR0XlfYcX27avgfpc1snG/WbtB1ZoWBLv2uSKLrPAxoomZ82z2fh3W4pq3Olo1Kq8qZ7wAgLeqttx4biYc7VZYZDUMr5466/ZXYKJ5Mvis=");
        this.architectView.onCreate(config);
        getLocalization();
    }
    private void getLocalization() {

        final Activity activity = this;

        // Obtenemos una referencia al LocationManager
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Nos registramos para recibir actualizaciones de la posición
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }

                Random rand = new Random();

                int randomNum = rand.nextInt(50);

                Toast.makeText(
                        activity,
                        "nueva posicion obtenida " + latitude + " " + longitude + " "
                                + randomNum, Toast.LENGTH_SHORT).show();

                architectView.setLocation(latitude, longitude, 1f);
            }
            @Override
            public void onProviderDisabled(String provider) {
                Toast.makeText(activity, "Debe de habilitar el \"Acceso a su ubicación\" ",
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onStatusChanged(String provider, int status,
                                        Bundle extras) {

                // muestro un mensaje segun el estatus erroneo de la senal
                switch (status) {
                    case LocationProvider.OUT_OF_SERVICE:
                        Toast.makeText(
                                activity,
                                "Se a perdido la comunicación con el GPS y/o redes teléfonicas.",
                                Toast.LENGTH_LONG).show();
                        break;
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        Toast.makeText(
                                activity,
                                "Se a perdido la comunicación con el GPS y/o redes teléfonicas.",
                                Toast.LENGTH_LONG).show();
                        break;
                    case LocationProvider.AVAILABLE:
                        Toast.makeText(
                                activity,
                                "Se a establecido la conexión con el GPS y/o redes teléfonicas.",
                                Toast.LENGTH_LONG).show();
                        break;
                }

            }
        };

        // Si esta habilitada el GPS en el dispositivo
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            // obtengo la ultima localizacion del usuario, si no la hay, es null

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }

            // actualizo la posicion del usuario cada 5 min = 80000 ms
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 80000, 0, locationListener);

        } else if (locationManager // Puntos Wifi o senal telefonica
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            // obtengo la ultima localizacion del usuario, si no la hay, es null
            Location location = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }

            // actualizo la posicion del usuario cada 5 min = 80000 ms
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 80000, 0,
                    locationListener);

        } else {
            // servicio desactivado
            Toast.makeText(
                    this,
                    "Por favor active el \"Acceso a su ubicación\" desde las configuraciones.",
                    Toast.LENGTH_LONG).show();

        }

    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.architectView.onPostCreate();
        try {
            this.architectView.load("Prueba/index.html");
            //this.architectView.load("08_Browsing$Pois_5_Native$Detail$Screen/index.html");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.architectView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.architectView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.architectView.onDestroy();
    }

}
