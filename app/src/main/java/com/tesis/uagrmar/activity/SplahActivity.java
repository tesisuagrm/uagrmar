package com.tesis.uagrmar.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.tesis.uagrmar.R;

public class SplahActivity extends AppCompatActivity {


    private final int SPLASH_DISPLAY_LENGTH = 3000;

    public TextView txt_texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splah);
//        txt_texto = (TextView) findViewById(R.id.txt_texto);


        new Handler().postDelayed(new Runnable() {
            public void run() {
                selectActivity();
            }
        }, SPLASH_DISPLAY_LENGTH);


    }

    public void selectActivity() {
        Intent intent = new Intent(SplahActivity.this,
                MenuActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
