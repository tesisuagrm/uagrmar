package com.tesis.uagrmar.activity;

import android.content.Intent;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tesis.uagrmar.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MapsModuleActivity extends FragmentActivity implements OnMapReadyCallback, AdapterView.OnClickListener {

    private GoogleMap mMap;
    public ImageView item_image_categ;
    public List<Establecimiento> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_module);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initView();
    }

    public void initView() {
        ((FloatingActionButton)findViewById(R.id.acction_modulo)).setOnClickListener(this);
        ((FloatingActionButton)findViewById(R.id.action_fotocopiadora)).setOnClickListener(this);
        ((FloatingActionButton)findViewById(R.id.action_administrativa)).setOnClickListener(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     * 61:E8:50:18:AD:73:07:E6:F3:6A:82:CF:58:20:08:B1:8F:23:B3:3D
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng uagrm = new LatLng(-17.7759963, -63.1961457);

        mMap.addMarker(new MarkerOptions().position(uagrm).title("UAGRM"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(uagrm));


    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.acction_modulo) {
            JSONObject params=new JSONObject();
            try {
                params.put("id_category",1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray array=new JSONArray();
            array.put(params);
            new PostRequest(this,array,"http://192.168.100.75:3000/api/categoria?id_category=1").sendPost(this);



        } else if (v.getId() == R.id.action_fotocopiadora) {
            JSONObject params=new JSONObject();
            try {
                params.put("id_category",3);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray array=new JSONArray();
            array.put(params);
            new PostRequest(this,array,"http://192.168.43.16:3000/api/categoria?id_category=3").sendPost(this);

        } else if (v.getId() == R.id.action_administrativa) {
            JSONObject params=new JSONObject();
            try {
                params.put("id_category",5);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray array=new JSONArray();
            array.put(params);
            new PostRequest(this,array,"http://192.168.43.16:3000/api/categoria?id_category=5").sendPost(this);
        }

    }
    public void obtenerModulos(List<Establecimiento> lista){
        try{

            mMap.clear();
            Log.i("Info",String.valueOf(lista.size()));
            for (int i = 0; i < lista.size() ; i++) {
                LatLng uagrm = new LatLng(Double.parseDouble(lista.get(i).latitud), Double.parseDouble(lista.get(i).longitud));

                mMap.addMarker(new MarkerOptions().position(uagrm).title(lista.get(i).nombre));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(uagrm));
            }

        }
        catch (Exception e){}

    }
}